package com.example.spring_general_example.service;

import com.example.spring_general_example.common.Commons;
import com.example.spring_general_example.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = {UserService.class, BCryptPasswordEncoder.class})
public class UserServiceTest extends Commons {

    @MockBean
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    @Test
    void loadUserByUsernameTest() {
        when(userRepository.findByUsername(any())).thenReturn(getDummyUser());
        UserDetails userDetails = userService.loadUserByUsername("Some Name");
        assertThat(userDetails.getUsername()).isEqualTo("TestUser");
        assertThat(userDetails.getPassword()).isEqualTo("password");
    }

}
