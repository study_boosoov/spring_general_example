package com.example.spring_general_example.controller;

import com.example.spring_general_example.common.Commons;
import com.example.spring_general_example.mappers.UserMapping;
import com.example.spring_general_example.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest({UserController.class, UserMapping.class})
@AutoConfigureMockMvc(addFilters = false)
public class UserControllerTest extends Commons {

    @Autowired
    MockMvc mvc;

    @MockBean
    UserService userService;

    @Test
    void getUsersTest() throws Exception {
        when(userService.findAll()).thenReturn(List.of(getDummyUser()));
        mvc.perform(get("/users"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()").value(1))
                .andExpect(jsonPath("$[0].username").value("TestUser"))
                .andExpect(jsonPath("$[0].password").value("password"));
    }

}
