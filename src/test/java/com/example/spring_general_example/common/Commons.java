package com.example.spring_general_example.common;

import com.example.spring_general_example.model.User;
import com.example.spring_general_example.model.UserRole;

import java.util.Set;

public class Commons {
    protected User getDummyUser() {
        return new User()
                .setUsername("TestUser")
                .setPassword("password")
                .setEmail("mail")
                .setRoles(Set.of(new UserRole().setName("User")));
    }
}
