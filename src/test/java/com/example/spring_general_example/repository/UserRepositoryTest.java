package com.example.spring_general_example.repository;

import com.example.spring_general_example.Application;
import com.example.spring_general_example.common.Commons;
import com.example.spring_general_example.model.User;
import com.example.spring_general_example.model.UserRole;
import org.h2.tools.Server;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.SQLException;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class UserRepositoryTest extends Commons {

    @Autowired
    UserRepository userRepository;

    @Before
    public void initTest() throws SQLException {
        Server.createWebServer("-web", "-webAllowOthers", "-webPort", "8082")
                .start();
    }

    @Test
    public void savaFindDeleteTest() {
        assertThat(userRepository.findAll().size()).isEqualTo(2);
        userRepository.save(getDummyUser());
        assertThat(userRepository.findAll().size()).isEqualTo(3);

        User user = userRepository.findByUsername("TestUser");
        assertThat(user.getUsername()).isEqualTo("TestUser");
        assertThat(user.getPassword()).isEqualTo("password");
        assertThat(user.getEmail()).isEqualTo("mail");
        assertThat(user.getRoles().size()).isEqualTo(1);
        assertThat(user.getRoles().contains(new UserRole().setName("User"))).isEqualTo(true);

        userRepository.deleteById(user.getId());
        List<User> users = userRepository.findAll();
        assertThat(users.size()).isEqualTo(2);
        assertThat(users.get(0).getUsername()).isEqualTo("user");
        assertThat(users.get(1).getUsername()).isEqualTo("admin");
    }
}
