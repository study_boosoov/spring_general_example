package com.example.spring_general_example.mappers;

import com.example.spring_general_example.model.User;
import com.model.UserDto;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel="spring")
public interface UserMapping {
    UserDto mapToUserDto(User user);
    List<UserDto> mapToUserDto(List<User> user);
    User mapToUser(UserDto userDto);
    List<User> mapToUser(List<UserDto> userDtos);
}
