package com.example.spring_general_example.controller;

import com.example.spring_general_example.model.User;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
public class WelcomeController {

    @GetMapping("/")
    public String welcomeRoot() {
        return "Welcome to /";
    }

    @GetMapping("/home")
    public String welcomeHome() {
        return "Welcome to /home";
    }

    @GetMapping("/user_home")
    public String welcomeUser(Authentication authentication) {
        User user = (User) Optional.ofNullable(authentication)
                .map(Authentication::getPrincipal)
                .orElse(null);
        return "Welcome to /home " + user.getUsername();
    }
}
