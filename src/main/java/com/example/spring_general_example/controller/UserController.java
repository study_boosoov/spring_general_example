package com.example.spring_general_example.controller;

import com.api.CreateUserApi;
import com.api.UserApi;
import com.api.UsersApi;
import com.example.spring_general_example.mappers.UserMapping;
import com.example.spring_general_example.model.User;
import com.example.spring_general_example.service.UserService;
import com.model.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.NativeWebRequest;

import java.util.List;
import java.util.Optional;

@RestController
public class UserController implements UserApi, UsersApi, CreateUserApi {

    @Autowired
    private UserMapping userMapping;

    @Autowired
    private UserService userService;

    @Override
    public ResponseEntity<List<UserDto>> getUsers() {
        ResponseEntity.BodyBuilder responseEntity  = ResponseEntity.status(200);
        List<User> users = userService.findAll();
        List<UserDto> usersDto = userMapping.mapToUserDto(users);
        return responseEntity.body(usersDto);
    }

    @Override
    public ResponseEntity<UserDto> getUserById(Long userId) {
        ResponseEntity.BodyBuilder responseEntity  = ResponseEntity.status(200);
        User users = userService.findById(userId);
        UserDto usersDto = userMapping.mapToUserDto(users);
        return responseEntity.body(usersDto);
    }

    @Override
    public ResponseEntity<UserDto> createUser(UserDto userDto) {
        ResponseEntity.BodyBuilder responseEntity  = ResponseEntity.status(200);
        User savedUser = userService.save(userMapping.mapToUser(userDto));
        return responseEntity.body(userMapping.mapToUserDto(savedUser));
    }

    @Override
    public ResponseEntity<Void> deleteUser(Long userId) {
        userService.deleteById(userId);
        return ResponseEntity.status(200).body(null);
    }

    @Override
    public Optional<NativeWebRequest> getRequest() {
        return UserApi.super.getRequest();
    }
}
