package com.example.spring_general_example.initialization;

import com.example.spring_general_example.model.Role;
import com.example.spring_general_example.model.User;
import com.example.spring_general_example.repository.RoleRepository;
import com.example.spring_general_example.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

@Component
public class ApplicationInitializationListener implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        System.out.println("Application initialized. Running post-init function...");
//        postInit();
//        User user = userRepository.findById(1L).get();
    }

    private void postInit() {
        insertInitData();
    }

    public void insertInitData() {
        Resource createRoleSqlResource = new ClassPathResource("sql/create-role.sql");
        String createRoleSql;
        try {
            createRoleSql = createRoleSqlResource.getContentAsString(StandardCharsets.UTF_8);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        jdbcTemplate.execute(createRoleSql);

        roleRepository.save(new Role().setName("ROLE_USER"));
        roleRepository.save(new Role().setName("ROLE_ADMIN"));
    }
}
