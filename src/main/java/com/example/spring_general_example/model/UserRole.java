package com.example.spring_general_example.model;

import org.springframework.data.relational.core.mapping.Table;

@Table("ROLE_TO_USER")
public class UserRole extends Role {

    @Override
    public UserRole setName(String name) {
        return (UserRole) super.setName(name);
    }
}