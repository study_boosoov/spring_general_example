package com.example.spring_general_example.scheduled;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class ScheduledTasks {

    @Transactional
    @Scheduled(fixedDelay = 2000)
    public void emulationUpdatingExchangeRate() {
        org.apache.logging.log4j.Logger logger = org.apache.logging.log4j.LogManager.getLogger();
        logger.info("This is an info message");
    }
}
