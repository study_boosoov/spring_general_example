package com.example.spring_general_example.repository;

import com.example.spring_general_example.model.User;
import org.springframework.data.jdbc.repository.query.Modifying;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

    User findByUsername(String username);
    List<User> findAll();

    @Modifying
    @Query("UPDATE user SET email = :email WHERE id = :id")
    boolean updateEmail(@Param("id") Long id, @Param("email") String email);

}
