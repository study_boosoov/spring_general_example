create table USERS (
    id int NOT NULL auto_increment,
    username varchar(255),
    password varchar(255),
    password_confirm varchar(255),
    email varchar(255),
    primary key (id)
);