create table ROLE_TO_USER (
    id int NOT NULL auto_increment,
    users_id int NOT NULL,
    name varchar(255) NOT NULL,
    primary key (id),
    foreign key (users_id) references users(id) ON delete cascade
);