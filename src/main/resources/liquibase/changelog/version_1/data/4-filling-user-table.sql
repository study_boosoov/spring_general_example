insert into USERS (username, password, email)
values  ('user', '$2a$10$910ME.t4XX8nnMzZKuIAOezn3YTcI6FStqRJovEO7nvp55rYX1hNC', 'user_mail'),
        ('admin', '$2a$10$sXnxukLWatHSpTdR8uJHXuQtYQIFxDkN0KJDHdFY9uZ.S5L3MtJ0.', 'admin_mail');