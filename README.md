# Spring general example



## Technology
- Spring Boot
- Spring Data JDBC
- Spring Security
- Spring MVC
- Spring Web
- Spring Test

## Resources
- Swagger UI - https://www.baeldung.com/spring-rest-openapi-documentation


### CONTRACT - http://localhost:8082/swagger-ui/index.html

### Login in Postman POST http://localhost:8082/login?username=user&password=user

user - user

admin - admin






## Notes
For connect ot H2 DB during tests you must

write in test while(true) {} and run this test

Open http://localhost:8082/

url - jdbc:h2:mem:db

login - sa

password - sa

